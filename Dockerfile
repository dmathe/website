FROM node:10.15.3 AS builder

RUN apt-get update && apt-get install build-essential

RUN mkdir /website
WORKDIR /website
COPY . .
RUN npm install
RUN npm run build

FROM nginx:stable-alpine

COPY ./docker/nginx-site.conf /etc/nginx/conf.d/site.conf
COPY --from=builder /website/dist /site

EXPOSE 81