import {
  BrowserRouter as Router, Route, Switch, Redirect,
} from 'react-router-dom';
import React, { Suspense, useState } from 'react';
import { IntlProvider } from 'react-intl';
import 'App.css';
import Layout from 'containers/Layout';
import ErrorBoundary from 'ErrorBoundary';
import { messages } from 'translations';

import routes from './routes';
import Language from './components/Language';


const App = () => {
  const [locale, setLocale] = useState('fr');

  const handleSelect = (e) => setLocale(e.target.value);

  return (
    <>
      <Language handleSelect={handleSelect} defaultValue={locale} />
      <Router>
        <IntlProvider locale={locale} messages={messages[locale]}>
          <Layout>
            <ErrorBoundary>
              <Suspense fallback={<div>Chargement...</div>}>
                <Switch>
                  {routes.map(({ name, path, component }) => (
                    <Route exact key={name} path={path} component={component} />
                  ))}
                  <Redirect to="/" />
                </Switch>
              </Suspense>
            </ErrorBoundary>
          </Layout>
        </IntlProvider>
      </Router>
    </>
  );
};

export default App;
