const styles = {
  colors: {
    white: 'white',
    // lightBlue: 'rgba(160, 203, 255, 0.18)',
    lightBlue: '#E6F0FA',
    selectBlue: '#8AAFDE',
    hoverBlue: '#D4E7FF',
    labelBlue: '#6C8AAF',
    darkBlue: '#385880',
    nightBlue: '#6C8AAF',
    middBlue: '#93AECF',
    disabled: '#DDDDDD',
    title: '#6C8AAF',
    description: '#929292',
    highlight: '#F8FBFF',
    lightGrey: 'lightgrey',
    checkBlue: '#B9D9FF',
  },
  fontSize: {},
  fontFamily: {
    h: 'Changa Bold',
    smallH: 'Changa',
  },
};

export default styles;
