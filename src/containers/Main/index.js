import React from 'react';
import Home from 'containers/Home';
import YourNeeds from 'containers/YourNeeds';
import JoinUs from 'containers/JoinUs';
import Contact from 'containers/Contact';
import { SectionStl } from 'containers/Layout';

const Main = () => (
  <>
    <SectionStl>
      <Home id="qui-sommes-nous" />
    </SectionStl>
    <SectionStl>
      <YourNeeds id="vos-besoins" />
    </SectionStl>
    <SectionStl>
      <JoinUs id="rejoignez-nous" />
    </SectionStl>
    <SectionStl>
      <Contact id="contact" />
    </SectionStl>
  </>
);

export default Main;
