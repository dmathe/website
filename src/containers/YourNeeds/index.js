import React from 'react';
import PropTypes from 'prop-types';
import SvgYourNeeds from 'images/SvgYourNeedsPage.svg';
import Svg from 'components/Svg';
import Title from 'components/Titles';
import Describe from 'components/Describe';
import ButtonLink from 'components/buttons/ButtonLink';
import { YourNeedsContainer, ContentContainer } from './styled';

const YourNeeds = ({ id }) => (
  <YourNeedsContainer id={id}>
    <Svg name="SvgYourNeeds" src={SvgYourNeeds} size={300} />
    <ContentContainer>
      <Title textAlign="right">Lorem Ipsum is simply</Title>
      <Describe>
        Lorem Ipsum is simply dummy text of the printing
        and typesetting industry.
        Lorem Ipsum has been the industry s ...
      </Describe>
      <ButtonLink txt="Suivant" to="/vos-besoins" />
    </ContentContainer>
  </YourNeedsContainer>
);

YourNeeds.propTypes = {
  id: PropTypes.string.isRequired,
};

export default YourNeeds;
