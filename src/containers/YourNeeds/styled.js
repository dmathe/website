import styled from 'styled-components';
import styles from 'global-styles';

const YourNeedsContainer = styled.div`
  display: flex;
  flex-flow: row;
  align-items: center;
  justify-content: space-evenly;
  width: auto;
  height: 100vh;
  background: ${styles.colors.lightblue};
`;

const ContentContainer = styled.div`
  height: 600px;
  margin: 15px;
  padding: 25px;
  width: 600px;
  display: flex;
  flex-flow: column;
  align-items: flex-end;
  position: relative;
`;

export {
  YourNeedsContainer,
  ContentContainer,
};
