import React from 'react';
import renderer from 'react-test-renderer';
import Contact from '..';

describe('Input', () => {
  test('snapshot renders', () => {
    const component = renderer.create(<Contact />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
