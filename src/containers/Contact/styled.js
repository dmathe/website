
import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100vh;
  width: auto;
`;

export const FormStl = styled.form`
  display: flex;
  max-width: 550px;
  height: auto;
  flex-direction: column;
`;

export const LineStl = styled.div`
  display: flex;
  width: 100%;
  height: auto;
  text-align: center;
  justify-content: space-around;
  /* flex-wrap: wrap; */
`;

export const Content = styled.div`
  width: 100%;
  height: auto;
  display: flex;
  justify-content: end;
  flex-direction: column;
  align-items: center;
`;
