import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Title from 'components/Titles';
import Input from 'components/Input';
import ButtonSimple from 'components/buttons/ButtonSimple';
import HeaderStl from 'components/HeaderStl/styled';
import {
  Wrapper, FormStl, LineStl, Content,
} from './styled';

const initialState = {
  firstName: '',
  lastName: '',
  email: '',
  object: '',
  message: '',
};

const Contact = ({ id }) => {
  const [state, setState] = useState(initialState);

  const handleChange = (value, name) => setState({ ...state, [name]: value });

  const handleSubmit = (e) => {
    e.preventDefault();
  };
  return (
    <Wrapper id={id}>
      <HeaderStl>
        <Title>Besoin d&apos;un renseignement ?</Title>
      </HeaderStl>
      <Content>

        <FormStl onSubmit={handleSubmit} autoComplete="off">
          <LineStl>
            <Input
              label="Prenom"
              type="text"
              name="firstName"
              value={state.firstName}
              onChange={handleChange}
            />
            <Input
              label="Nom"
              type="text"
              name="lastName"
              value={state.lastName}
              onChange={handleChange}
            />
          </LineStl>
          <LineStl>
            <Input
              label="Email"
              type="text"
              name="email"
              value={state.email}
              onChange={handleChange}
            />

          </LineStl>
          <LineStl>
            <Input
              label="Objet"
              type="text"
              name="object"
              value={state.object}
              onChange={handleChange}
            />

          </LineStl>
          <LineStl>
            <Input
              label="Message"
              type="textarea"
              name="message"
              value={state.message}
              onChange={handleChange}
            />
          </LineStl>
          <ButtonSimple txt="Envoyer" type="submit" margin="35px" />
        </FormStl>
      </Content>
    </Wrapper>
  );
};

Contact.propTypes = {
  id: PropTypes.string.isRequired,
};

export default Contact;
