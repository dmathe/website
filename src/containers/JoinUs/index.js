import React from 'react';
import PropTypes from 'prop-types';
import SvgJoinUs from 'images/SvgJoinUsPage.svg';
import Svg from 'components/Svg';
import Title from 'components/Titles';
import Describe from 'components/Describe';
import ButtonSimple from 'components/buttons/ButtonSimple';
import { JoinUsContainer, ContentContainer } from './styled';

const JoinUs = ({ id }) => (
  <JoinUsContainer id={id}>
    <Svg name="SvgJoinUs" src={SvgJoinUs} size={300} />
    <ContentContainer>
      <Title textAlign="right">Lorem Ipsum has been the industry</Title>
      <Describe>
        Lorem Ipsum is simply dummy text of the printing
        and typesetting industry.
        Lorem Ipsum has been the industry s ...
      </Describe>
      <ButtonSimple txt="Suivant" />
    </ContentContainer>
  </JoinUsContainer>
);

JoinUs.propTypes = {
  id: PropTypes.string.isRequired,
};

export default JoinUs;
