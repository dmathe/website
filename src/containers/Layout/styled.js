import styled from 'styled-components';
import styles from 'global-styles';

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-flow: row;
`;

export const LeftStl = styled.div`
  max-width: 250px;
  min-width: 250px;
`;

export const RightStl = styled.div`
  background: ${styles.colors.white};
  width: fill-available;
  overflow-y: scroll;
`;

export const LayoutContentStl = styled.div`
  padding: 0px 60px;
  height: ${({ numberOfElem }) => `calc(100%*${numberOfElem})`};
`;
