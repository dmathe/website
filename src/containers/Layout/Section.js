import styled from 'styled-components';

const Section = styled.div`
  display: flex;
  justify-content: center;
  height: calc(100%/4);
  min-width: fit-content;
`;

export default Section;
