import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import SideBar from '../../components/SideBar';
import Section from './Section';
import {
  Wrapper, LeftStl, RightStl, LayoutContentStl,
} from './styled';

const Layout = ({ children, location }) => (
  <Wrapper>
    <LeftStl>
      <SideBar />
    </LeftStl>
    <RightStl id="scrollbar">
      <LayoutContentStl numberOfElem={location.pathname !== '/' ? 1 : 4}>
        {children}
      </LayoutContentStl>
    </RightStl>
  </Wrapper>
);

Layout.propTypes = {
  children: PropTypes.element.isRequired,
  location: PropTypes.object.isRequired,
};

export const SectionStl = Section;

export default withRouter(Layout);
