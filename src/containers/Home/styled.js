import styled from 'styled-components';
import theme from 'global-styles';

export const Wrapper = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  position: relative;
  max-height: 100%;
`;

export const LogoStl = styled.img`
  margin: auto;
  max-width: 20%;
  min-width: 150px;
`;

export const MiddleContentStl = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
`;

export const LineStl = styled.div`
  height: 4px;
  width: 50%;
  background-color: ${theme.colors.lightBlue};
`;

export const ItemsStl = styled.div`
  width: 100%;
  display: flex;
  flex-flow: row;
  justify-content: space-between;
  margin: auto;
  min-height: 30%;
`;

export const NavBarStl = styled.a`
  width: calc(100% + 12vw);
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  background-color: ${theme.colors.highlight};
  font-size: 2em;
  padding-bottom: 1vh;
  opacity: 0.5;
  cursor: pointer;
  &:hover {
    opacity: 1;
  }
`;

export const NavTitleStl = styled.h5`
  margin: 0;
  color: ${theme.colors.labelBlue};
  font-family: ${theme.fontFamily.smallH};
`;
