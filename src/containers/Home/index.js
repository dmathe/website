import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Title from 'components/Titles';
import Card from 'components/Card';
import Description from 'components/DescriptionService';
import logo from 'images/Logo.svg';
import DescribeStl from 'components/Describe';
import ArrowDown from 'images/arrowDown.svg';
import cards from './cardsContent';
import {
  Wrapper,
  LogoStl,
  MiddleContentStl,
  ItemsStl,
  LineStl,
  NavBarStl,
  NavTitleStl,
} from './styled';

const initialState = {
  selected: null,
  unSelected: null,
  position: 0,
};

const filterCards = ({ title }) => cards.filter((card) => card.title !== title);

const Home = ({ id }) => {
  const [state, setState] = useState(initialState);

  const handleClickItem = (selected) => {
    if (!selected) setState(initialState);
    else {
      const unSelected = filterCards(selected);
      setState({ unSelected, selected });
    }
  };

  return (
    <Wrapper id={id}>
      <LogoStl src={logo} />
      <MiddleContentStl>
        <Title style={{ margin: '0' }}> Votre service du numerique </Title>
        <DescribeStl align="center">
          Lorem Ipsum is simply dummy text of the printing
          and typesetting industry. Lorem Ipsum
        </DescribeStl>
      </MiddleContentStl>
      <LineStl />
      <ItemsStl>
        {state.selected
          ? (
            <Description
              onClick={handleClickItem}
              {...state.selected}
              unSelected={state.unSelected}
            />
          )
          : cards.map((card) => (
            <Card
              key={card.title}
              onClick={() => handleClickItem(card)}
              {...card}
            />
          ))}
      </ItemsStl>
      <NavBarStl smooth href="#vos-besoins">
        <NavTitleStl>Decouvrir nos services</NavTitleStl>
        <img alt="navigation" src={ArrowDown} />
      </NavBarStl>
    </Wrapper>
  );
};

Home.propTypes = {
  id: PropTypes.string.isRequired,
};

export default Home;
