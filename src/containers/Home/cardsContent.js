
import contact from 'images/contact.svg';
import dashboard from 'images/dashboard.svg';
import diamond from 'images/diamond.svg';

const cardsContent = [
  {
    title: 'EFFICACITE',
    icon: dashboard,
    description: `Fort de notre experience et de notre savoir-faire,
     l’equipe Whis repond a votre besoin avec efficacite et rapidite.`,
  },
  {
    title: 'QUALITE',
    icon: diamond,
    description: `Lorem Ipsum is simply dummy text of the printing and
      typesetting industry. Lorem Ipsum`,
  },
  {
    title: 'SUIVI',
    icon: contact,
    description: `Lorem Ipsum is simply dummy text of the printing and
      typesetting industry. Lorem Ipsum`,
  },
];

export default cardsContent;
