import React from 'react';
import renderer from 'react-test-renderer';
import { IntlProvider } from 'react-intl';
import Home from '..';

describe('Home', () => {
  test('snapshot renders', () => {
    const component = renderer.create(
      <IntlProvider locale="fr">
        <Home id="qui-sommes-nous" />
      </IntlProvider>,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
