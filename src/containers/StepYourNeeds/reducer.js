const initialState = {
  currentStep: 1,
  totalStep: 4,
  startedStep: 1,
  wayOfState: 0,
  formStateStep: {},
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'goNextStep':
      return {
        ...state,
        currentStep: state.currentStep + 1,
        startedStep: state.currentStep + 1,
      };
    case 'selectStep': {
      return {
        ...state,
        currentStep: action.index,
      };
    }
    case 'setWayOfStep': {
      const changeWay = action.way !== state.wayOfState;
      const nextCurrentStep = state.currentStep + 1;
      return {
        ...state,
        wayOfState: action.way,
        currentStep: nextCurrentStep,
        startedStep: nextCurrentStep,
        formStateStep: changeWay ? {} : state.formStateStep,
      }; }

    case 'initStep': {
      let newFormStateStep = { ...state.formStateStep };
      newFormStateStep = { ...newFormStateStep, [state.currentStep]: action.payload };
      return {
        ...state,
        formStateStep: newFormStateStep,
      }; }
    case 'setStep': {
      const newFormStateStep = { ...state.formStateStep };
      newFormStateStep[state.currentStep][action.name] = action.value;
      return {
        ...state,
        formStateStep: newFormStateStep,
        currentStep: action.changeStep ? state.currentStep + 1 : state.currentStep,
        startedStep: action.changeStep ? state.startedStep + 1 : state.startedStep,
      };
    }
    default:
      throw new Error();
  }
};

export { initialState, reducer };
