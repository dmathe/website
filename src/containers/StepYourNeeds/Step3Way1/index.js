import React, { useContext, useEffect } from 'react';
import ButtonSelection from 'components/buttons/ButtonSelection';
import LowLvl from 'images/lowLevel.svg';
import MiddleLvl from 'images/middleLevel.svg';
import HighLvl from 'images/highLevel.svg';
import {
  Wrapper, TitleStl, TextSubTitleStl, TextTitleStl, SelectionsStl, SubTitleStl, ChoicesStl,
} from './styled';
import StepContext from '../Context';
import { SET_STEP, INIT_STEP } from '../actions';

const initialData = {
  skills: '',
  available: '',
};

const buttonsSelection = [
  {
    name: 'Junior (< 2ans)',
    size: 's',
    svg: LowLvl,
  },
  {
    name: 'Confirme (2 a 5ans)',
    size: 's',
    svg: MiddleLvl,
  },
  {
    name: 'Expert (> 5ans)',
    size: 's',
    svg: HighLvl,
    margin: '10px',
  },
];

const Step3 = () => {
  const { state: { formStateStep, currentStep }, dispatch } = useContext(StepContext);

  let state = initialData;

  useEffect(() => {
    if (!formStateStep[currentStep]) dispatch({ type: INIT_STEP, payload: initialData });
    else state = formStateStep[currentStep];
  }, []);

  return (
    <Wrapper>
      <TitleStl>
        <TextTitleStl>Informations</TextTitleStl>
      </TitleStl>
      <SelectionsStl>
        <SubTitleStl><TextSubTitleStl>Experience</TextSubTitleStl></SubTitleStl>
        <ChoicesStl>
          {buttonsSelection.map((el) => (
            <ButtonSelection
              {...el}
              key={el.name}
              check
              selected={state.skills === el.name}
              onClick={() => dispatch({
                type: SET_STEP,
                name: 'skills',
                value: el.name,
                changeStep: true,
              })}
            />
          ))}
        </ChoicesStl>
        <SubTitleStl><TextSubTitleStl>Disponibilite</TextSubTitleStl></SubTitleStl>
      </SelectionsStl>
    </Wrapper>
  );
};

export default Step3;
