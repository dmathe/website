import styled from 'styled-components';
import styles from 'global-styles';

export const TitleStl = styled.div`
    display: flex;
    margin: 15px;
`;

export const SubTitleStl = styled.div`
    display: flex;
    width: 100%;
`;

export const ChoicesStl = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 90%;
    justify-content: center;
`;

export const TextSubTitleStl = styled.span`
    font-size: 21px;
    color: ${styles.colors.labelBlue};
    font-family: 'Changa';
`;

export const TextTitleStl = styled.span`
    font-size: 25px;
    color: ${styles.colors.labelBlue};
    font-family: 'Changa';
`;

export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 100%;
`;

export const SelectionsStl = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 90%;
`;
