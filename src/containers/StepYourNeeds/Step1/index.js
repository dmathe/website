import React, { useContext } from 'react';
import ButtonSelection from 'components/buttons/ButtonSelection';
import SvgTeam from 'images/teamMini.svg';
import SvgProject from 'images/rocketMini.svg';
import {
  Wrapper, SubTitleStl, TextSubTitleStl, SelectionsStl,
} from './styled';
import StepContext from '../Context';
import { SET_WAY_OF_STATE } from '../actions';

const buttonsSelection = [
  {
    name: 'Renforcer mon equipe',
    svg: SvgTeam,
    size: 'm',
    way: 0,
  }, {
    name: 'Realiser un projet',
    svg: SvgProject,
    size: 'm',
    way: 1,
  },
];

const Step1 = () => {
  const { dispatch } = useContext(StepContext);
  return (
    <Wrapper>
      <SubTitleStl>
        <TextSubTitleStl>Que recherchez-vous ?</TextSubTitleStl>
      </SubTitleStl>
      <SelectionsStl>
        {buttonsSelection.map((el) => (
          <ButtonSelection
            {...el}
            key={el.name}
            onClick={() => dispatch({ type: SET_WAY_OF_STATE, way: el.way })}
            arrow
          />
        ))}
      </SelectionsStl>
    </Wrapper>
  );
};

export default Step1;
