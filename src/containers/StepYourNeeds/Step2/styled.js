import styled from 'styled-components';
import styles from 'global-styles';

export const TitleStl = styled.div`
    display: flex;
    margin: 15px;
`;

export const TextSubTitleStl = styled.span`
    font-size: 25px;
    color: ${styles.colors.labelBlue};
    font-family: 'Changa';
`;

export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 100%;
`;

export const SelectionsStl = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-top: 20px;
`;

export const WrapTagStl = styled.div`
    display: flex;
    width: 100%;
    height: auto;
    text-align: center;
    flex-wrap: wrap;
`;

export const MinMaxSelectStl = styled.span`
    color: ${styles.colors.description};
    font-size: 14px;
    font-family: 'Roboto';
    text-align: right;
    margin-right: 10px;
`;
