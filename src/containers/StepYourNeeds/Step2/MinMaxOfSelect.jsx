import React, { memo } from 'react';
import { MinMaxSelectStl } from './styled';

const MinMaxOfSelect = memo(() => (
  <>
    <MinMaxSelectStl>
      Minimum de selections:
      {' '}
      <b>1</b>
    </MinMaxSelectStl>
    <MinMaxSelectStl>
      Maximum de selections:
      {' '}
      <b>5</b>
    </MinMaxSelectStl>
  </>
));
export default MinMaxOfSelect;
