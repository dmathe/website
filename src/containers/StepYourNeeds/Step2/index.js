import React, { useContext, useEffect, useCallback } from 'react';
import Select from 'components/Select';
import Tag from 'components/Tag';
import { LineStl, FormStl } from 'containers/Contact/styled';
import ButtonSimple from 'components/buttons/ButtonSimple';
import {
  Wrapper, TitleStl, TextSubTitleStl, WrapTagStl,
} from './styled';
import StepContext from '../Context';
import { INIT_STEP, SET_STEP, GO_NEXT_STEP } from '../actions';
import { dataList, skillsList } from './fakeData';
import MinMaxOfSelect from './MinMaxOfSelect';

const initialData = {
  poste: '',
  skillName: '',
  skillTags: [],
};

const Step2 = () => {
  const { state: { formStateStep, currentStep }, dispatch } = useContext(StepContext);

  let state = initialData;

  useEffect(() => {
    if (!formStateStep[currentStep]) dispatch({ type: INIT_STEP, payload: initialData });
    else state = formStateStep[currentStep];
  }, []);

  const dispatchChange = useCallback((n, v) => dispatch({
    type: SET_STEP,
    name: n,
    value: v,
  }), [SET_STEP]);

  const dispatchChangeStep = (e) => {
    e.preventDefault();
    dispatch({ type: GO_NEXT_STEP });
  };

  const isDisable = !(state.poste !== '' && state.skillTags.length);
  return (
    <Wrapper>
      <TitleStl>
        <TextSubTitleStl>Type de poste</TextSubTitleStl>
      </TitleStl>
      <FormStl onSubmit={dispatchChangeStep}>
        <LineStl>
          <Select
            onChange={(dispatchChange)}
            value={state.poste}
            dataList={dataList}
            label="Nom du poste"
            name="poste"
          />
        </LineStl>
        <LineStl>
          <Select
            tagsList={state.skillTags}
            onChange={dispatchChange}
            value={state.skillName}
            dataList={skillsList}
            label="Competences"
            name="skillName"
            tagsName="skillTags"
          />
        </LineStl>
        <MinMaxOfSelect />
        <WrapTagStl>
          {state.skillTags.map((el) => (
            <Tag
              key={el}
              onChange={dispatchChange}
              tagsList={state.skillTags}
              value={el}
              tagsName="skillTags"
            />
          ))}
        </WrapTagStl>
        <ButtonSimple disabled={isDisable} txt="Suivant" type="submit" margin="35px" />
      </FormStl>
    </Wrapper>
  );
};

export default Step2;
