import React, { useContext, useCallback, useEffect } from 'react';
import ButtonSelection from 'components/buttons/ButtonSelection';
import SvgSiteVitrine from 'images/siteVitrine.svg';
import SvgEcommerce from 'images/ecommerce.svg';
import SvgCms from 'images/cms.svg';
import {
  Wrapper, SubTitleStl, TextSubTitleStl, SelectionsStl,
} from './styled';
import StepContext from '../Context';
import { SET_STEP, INIT_STEP } from '../actions';

const initialData = {
  project: '',
};

const buttonsSelection = [
  {
    name: 'Site vitrine',
    svg: SvgSiteVitrine,
    size: 'm',
  },
  {
    name: 'E-commerce',
    svg: SvgEcommerce,
    size: 'm',
  },
  {
    name: 'CMS',
    svg: SvgCms,
    size: 'm',
  },
];

const Step2Way1 = () => {
  const { state: { formStateStep, currentStep }, dispatch } = useContext(StepContext);
  let state = initialData;

  useEffect(() => {
    if (!formStateStep[currentStep]) dispatch({ type: INIT_STEP, payload: initialData });
    else state = formStateStep[currentStep];
  }, []);

  return (
    <Wrapper>
      <SubTitleStl>
        <TextSubTitleStl>Type de projet</TextSubTitleStl>
      </SubTitleStl>
      <SelectionsStl>
        {buttonsSelection.map((el) => (
          <ButtonSelection
            {...el}
            key={el.name}
            selected={state.project === el.name}
            onClick={useCallback(() => dispatch({
              type: SET_STEP,
              name: 'project',
              value: el.name,
              changeStep: true,
            }), [SET_STEP])}
            arrow
          />
        ))}
      </SelectionsStl>
    </Wrapper>
  );
};

export default Step2Way1;
