export const SELECT_STEP = 'selectStep';
export const SET_WAY_OF_STATE = 'setWayOfStep';
export const SET_STEP = 'setStep';
export const GO_NEXT_STEP = 'goNextStep';
export const INIT_STEP = 'initStep';
