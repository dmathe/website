import React from 'react';
import renderer from 'react-test-renderer';
import StepYourNeeds from '../index';

describe('StepYourNeeds', () => {
  test('snapshot renders', () => {
    const component = renderer.create(<StepYourNeeds />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
