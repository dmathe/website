import React, { useReducer, useMemo } from 'react';
import Title from 'components/Titles';
import HeaderStl from 'components/HeaderStl/styled';
import LitleLight from 'images/littleLight.svg';
import StepBar from 'components/StepBar';
import { WrapperStep, ContentStep, SvgStl } from './styled';
import Step1 from './Step1';
import Step2 from './Step2';
import Step2Way1 from './Step2Way1';
import Step3Way1 from './Step3Way1';
import Step3 from './Step3';
import Step4 from './Step4';
import StepContext from './Context';
import { reducer, initialState } from './reducer';

const displayStep = (i) => [
  <Step1 />,
  !i ? <Step2 /> : <Step2Way1 />,
  !i ? <Step3 /> : <Step3Way1 />,
  <Step4 />,
];

const StepYourNeeds = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const { currentStep, wayOfState } = state;

  const contextValue = useMemo(() => ({ state, dispatch }), [state, dispatch]);

  return (
    <WrapperStep>
      <HeaderStl>
        <Title>Aidez-nous a identifier votre besoin</Title>
      </HeaderStl>
      <StepContext.Provider value={contextValue}>
        <StepBar />
        <ContentStep>
          {displayStep(wayOfState)[currentStep - 1]}
        </ContentStep>
      </StepContext.Provider>
      <SvgStl alt="litleLight" src={LitleLight} size={100} />
    </WrapperStep>
  );
};

export default StepYourNeeds;
