
import styled from 'styled-components';

export const WrapperStep = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const ContentStep = styled.div`
  margin-top: 15px;
`;

export const SvgStl = styled.img`
  height: ${({ size }) => `${size}px`};
  width: auto;
  position: absolute;
  bottom: 3%;
  right: 3%;
`;
