import React, { useContext, useEffect } from 'react';
import { FormStl, LineStl } from 'containers/Contact/styled';
import Input from 'components/Input';
import ButtonSimple from 'components/buttons/ButtonSimple';
import {
  Wrapper, TitleStl, TextTitleStl,
} from './styled';
import StepContext from '../Context';
import { SET_STEP, GO_NEXT_STEP, INIT_STEP } from '../actions';

const initialData = {
  firstName: '',
  lastName: '',
  societe: '',
  siret: '',
  email: '',
};

const listInputProps = [
  {
    label: 'Prenom',
    name: 'firstName',
  },
  {
    label: 'Nom',
    name: 'lastName',
  },
  {
    label: "Nom de l'entreprise",
    name: 'societe',
  },
  {
    label: 'SIRET',
    name: 'siret',
  },
  {
    label: 'Email',
    name: 'email',
  },
];

const Step4 = () => {
  const { state: { formStateStep, currentStep }, dispatch } = useContext(StepContext);
  let state = initialData;

  useEffect(() => {
    if (!formStateStep[currentStep]) dispatch({ type: INIT_STEP, payload: initialData });
    else state = formStateStep[currentStep];
  }, []);


  const dispatchChangeStep = (e) => {
    e.preventDefault();
    dispatch({ type: GO_NEXT_STEP });
  };

  const isDisable = Object.values(state).find((val) => val === '') !== undefined;

  return (
    <Wrapper>
      <TitleStl>
        <TextTitleStl>Qui etes-vous ?</TextTitleStl>
      </TitleStl>
      <FormStl onSubmit={dispatchChangeStep} autoComplete="off">
        {listInputProps.map((el) => (
          <LineStl key={el.name}>
            <Input
              {...el}
              value={state[el.name]}
              onChange={(n, v) => dispatch({ type: SET_STEP, name: n, value: v })}
            />
          </LineStl>
        ))}
        <ButtonSimple disabled={isDisable} txt="Suivant" type="submit" margin="35px" />
      </FormStl>

    </Wrapper>
  );
};

export default Step4;
