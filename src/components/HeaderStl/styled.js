import styled from 'styled-components';

const Header = styled.div`
  width: 100%;
  margin-top: 15px;
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

export default Header;
