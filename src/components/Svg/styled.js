import styled from 'styled-components';

const StyledSvg = styled.img`
  width: auto;
  height: ${({ size }) => `${size}px`};
  margin: 0 3%;
  cursor: ${({ cursor }) => (cursor || 'default')};
`;

export default StyledSvg;
