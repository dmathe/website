import React from 'react';
import PropTypes from 'prop-types';
import StyledSvg from './styled';

// size in px.

const Svg = ({
  name, src, size, cursor, onClick,
}) => (
  <StyledSvg onClick={onClick} alt={name} src={src} size={size} cursor={cursor} />
);

Svg.propTypes = {
  name: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
  cursor: PropTypes.string,
  onClick: PropTypes.func,
};

Svg.defaultProps = {
  cursor: 'default',
  onClick: () => {},
};

export default Svg;
