import React from 'react';
import PropTypes from 'prop-types';
import {
  Wrapper,
  // CheckpointStl,
  LineStl,
} from './styled';

const Stepper = ({ len, position }) => {
  const percentage = (position / (len - 1 || position)) * 100;
  // for (let index = 0; index < len; index++);
  // }
  return (
    <Wrapper>
      <LineStl percentage={percentage} />
    </Wrapper>
  );
};

Stepper.defaultProps = {
  // indications: '',
};

Stepper.propTypes = {
  len: PropTypes.number.isRequired,
  position: PropTypes.number.isRequired,
  // indications: PropTypes.number,
};

export default Stepper;
