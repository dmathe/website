import styled from 'styled-components';
import theme from 'global-styles';

export const Wrapper = styled.div`
  width: 100%;
  height: ${({ height }) => (height || '100%')};
  position: relative;
  display: flex;
  flex-flow: row;
  justify-content: space-between;
`;


export const CheckpointStl = styled.div`
  width: ${({ size }) => (`${size}vh` || '3vh')};
  height: ${({ size }) => (`${size}vh` || '3vh')};
  margin: auto 0;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 25px;
  z-index: 2;
  cursor: pointer;
  padding: 5px;
  border: 3px solid white;
  background: ${({ bgcolor }) => theme.colors[bgcolor] || `${theme.colors.lightGrey}`};
`;

export const LineStl = styled.div`
  width: 100%;
  height: 0.5vh;
  position: absolute;
  margin: auto;
  top: 0;
  bottom: 0;
  border-radius: 10px;
  background: ${({ percentage = 0 }) => `
    linear-gradient(90deg, #6C8AAF ${percentage}%, lightgrey ${percentage}%);
  `
}
`;
