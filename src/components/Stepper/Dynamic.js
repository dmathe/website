import React from 'react';
import PropTypes from 'prop-types';
import {
  Wrapper,
  CheckpointStl,
  LineStl,
} from './styled';

const Stepper = ({ len, position, onClick }) => {
  const percentage = (position / (len - 1 || position)) * 100;
  let Checkpoints = [];
  for (let index = 0; index < len; index++) {
    const color = (position === index) ? 'nightBlue' : 'checkBlue';
    const size = position === index ? 2 : 1;
    Checkpoints = [...Checkpoints, (
      <CheckpointStl
        bgcolor={position >= index ? color : 'grey'}
        key={`${index}`}
        size={size}
        onClick={() => onClick(index)}
      />
    )];
  }
  return (
    <Wrapper>
      <LineStl percentage={percentage} />
      {[...Checkpoints]}
    </Wrapper>
  );
};

Stepper.defaultProps = {
};

Stepper.propTypes = {
  len: PropTypes.number.isRequired,
  position: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Stepper;
