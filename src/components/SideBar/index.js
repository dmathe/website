import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import LogoWhis from 'images/logoWhis.svg';
import {
  ImgStl, Wrapper, SideNavStl, TextStl, BarStl, ButtonStl,
} from './styled';
import {
  newScrollPosition, minPosition, barPosition, isActive,
} from './functions';

const IntlMessage = (id) => <FormattedMessage id={id} />;

const listNav = [
  { name: IntlMessage('Nav1'), path: '#qui-sommes-nous' },
  { name: IntlMessage('Nav2'), path: '#vos-besoins' },
  { name: IntlMessage('Nav3'), path: '#rejoignez-nous' },
  { name: IntlMessage('Nav4'), path: '#contact' }];

const SideBar = ({ location }) => {
  const myClickedRef = React.useRef();
  const setMyClicked = (bool) => { myClickedRef.current = bool; };

  useEffect(() => {
    let scrollPercent;
    document.getElementById('scrollbar').addEventListener('scroll', ({ target }) => {
      if (window.location.pathname !== '/') return;
      if (myClickedRef.current) { // disable temporaly scroll event if link clicked on sidebar
        const newPosition = newScrollPosition(document.getElementById('scrollbar'));
        if (newPosition === minPosition(window.location.hash, listNav)) setMyClicked(false);
        return;
      }
      const newPosition = newScrollPosition(target);
      if (newPosition !== scrollPercent) {
        scrollPercent = newPosition;
        window.location.hash = listNav[barPosition(newPosition)].path;
      }
    });
  }, []);
  return (
    <Wrapper>
      <ImgStl src={LogoWhis} alt="logo whis" />
      <SideNavStl>
        {listNav.map(({ name, path }) => (
          <ButtonStl
            smooth
            to={`/${path}`}
            onClick={() => setMyClicked(true)}
            key={path}
          >
            <BarStl active={isActive(location, path)} />
            <TextStl active={isActive(location, path)}>{name}</TextStl>
          </ButtonStl>
        ))}
      </SideNavStl>
    </Wrapper>
  );
};

SideBar.propTypes = {
  location: PropTypes.object.isRequired,
};

export default withRouter(SideBar);
