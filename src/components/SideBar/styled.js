import styled from 'styled-components';
import styles from 'global-styles';
import { HashLink as Link } from 'react-router-hash-link';

export const ImgStl = styled.img`
  position: absolute;
  width: 145px;
  top: 60px;
  right: 0;
  left: 0;
  margin: 0 auto;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  position: relative;
  width: 100%;
  background-color: ${styles.colors.lightBlue};
  height: 100%;
`;

export const SideNavStl = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 10px 0;
`;

export const TextStl = styled.div`
  font-family: 'Changa';
  color: ${styles.colors.darkBlue};
  font-size: 20px;
  text-align: center;
  width: 212px;
  height: 90px;
  line-height: 90px;
  ${({ active }) => (active && `background: ${styles.colors.hoverBlue}`)};
`;

export const BarStl = styled.div`
  display: ${({ active }) => (active ? 'block' : 'none')};
  width: 5px;
  height: 90px;
  background-color: ${styles.colors.darkBlue};
  margin: auto 0;
  box-shadow: 1px 1px 4px 1px rgb(202, 202, 202);
  border-radius: 5px;
`;

export const ButtonStl = styled(Link)`
  display: flex;
  justify-content: center;
  cursor: pointer;
`;
