export const barPosition = (pos) => {
  if (pos >= 80 && pos < 160) return 1;
  if (pos >= 160 && pos < 240) return 2;
  if (pos >= 240 && pos <= 300) return 3;
  return 0;
};

export const minPosition = (name, listNav) => {
  if (name === listNav[1].path) return 100;
  if (name === listNav[2].path) return 200;
  if (name === listNav[3].path) return 300;
  return 0;
};

export const isActive = (location, path) => (location.hash === path
    || location.pathname.replace('/', '#') === path);


export const newScrollPosition = ({ scrollTop, offsetHeight }) => (
  Math.round((scrollTop / offsetHeight) * 100)
);
