import { barPosition, minPosition } from '../functions';

const mockListNav = [
  { name: 'Nav1', path: '#qui-sommes-nous' },
  { name: 'Nav2', path: '#vos-besoins' },
  { name: 'Nav3', path: '#rejoignez-nous' },
  { name: 'Nav4', path: '#contact' }];

describe('SideBar', () => {
  test('barPosition test', () => {
    expect(barPosition(0)).toEqual(0);
    expect(barPosition(80)).toEqual(1);
    expect(barPosition(160)).toEqual(2);
    expect(barPosition(240)).toEqual(3);
    expect(barPosition(301)).toEqual(0);
  });
  test('minPosition test', () => {
    expect(minPosition('#qui-sommes-nous', mockListNav)).toEqual(0);
    expect(minPosition('#vos-besoins', mockListNav)).toEqual(100);
    expect(minPosition('#rejoignez-nous', mockListNav)).toEqual(200);
    expect(minPosition('#contact', mockListNav)).toEqual(300);
    expect(minPosition('#sfvsdv', mockListNav)).toEqual(0);
  });
});
