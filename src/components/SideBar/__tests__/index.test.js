import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import { messages } from 'translations';
import { mount } from 'enzyme';
import SideBar from '../index';

const defaultProps = {
  location: { hash: '#qui-sommes-nous' },
};

describe('SideBar', () => {
  test('snapshot renders', () => {
    const scrollbar = document.createElement('div');
    scrollbar.setAttribute('id', 'scrollbar');
    document.body.appendChild(scrollbar);
    jest.spyOn(React, 'useRef').mockReturnValueOnce({ current: false });

    const component = mount(
      <MemoryRouter initialEntries={['/#qui-sommes-nous']}>
        <IntlProvider locale="fr" messages={messages.fr}>
          <SideBar props={defaultProps} />
        </IntlProvider>
      </MemoryRouter>,
      { attachTo: scrollbar },
    );
    scrollbar.dispatchEvent(new Event('scroll'));
    expect(component.find('Wrapper')).toMatchSnapshot();
  });
});
