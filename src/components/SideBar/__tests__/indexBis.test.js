import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import { messages } from 'translations';
import { mount } from 'enzyme';
import SideBar from '../index';
import { newScrollPosition, minPosition } from '../functions';

jest.mock('../functions');

const defaultProps = {
  location: { hash: '#qui-sommes-nous' },
};

describe('SideBar', () => {
  test('snapshot renders with clicked true', () => {
    const scrollbar = document.createElement('div');
    scrollbar.setAttribute('id', 'scrollbar');
    document.body.appendChild(scrollbar);

    newScrollPosition.mockReturnValue(0);
    minPosition.mockReturnValue(0);

    jest.spyOn(React, 'useRef').mockReturnValue({ current: true });


    const component = mount(
      <MemoryRouter initialEntries={['/#qui-sommes-nous']}>
        <IntlProvider locale="fr" messages={messages.fr}>
          <SideBar props={defaultProps} />
        </IntlProvider>
      </MemoryRouter>,
      { attachTo: scrollbar },
    );
    expect(component.find('Wrapper')).toMatchSnapshot();
    scrollbar.dispatchEvent(new Event('scroll'));
    const click = component.find('a').at(0);
    click.simulate('click');
  });
});
