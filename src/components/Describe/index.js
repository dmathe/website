import styled from 'styled-components';

export default styled.h3`
  font-family: 'Changa';
  font-style: normal;
  font-size: 30px;
  line-height: 47px;
  /* or 157% */

  display: flex;
  align-items: center;
  text-align: ${({ align }) => align || 'right'};
  letter-spacing: 0.5px;

  color: #D3D3D3;
  width: 80%;
`;
