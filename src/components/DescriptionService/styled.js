import styled from 'styled-components';

export const DescriptionWrapStl = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  max-width: 70%;
`;
export const DescContentStl = styled.div`
  margin: auto;
  color: grey;
  font-size: 1.5em;
  text-align: center;
  max-width: 70%;
`;


export const ItemsRightStl = styled.div`
  max-width: 30%;
  display: flex;
  flex-flow: column;
  align-items: center;
  justify-content: center;
  max-height: 80%;
  margin: auto 0;
`;
