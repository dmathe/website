import React from 'react';
import PropTypes from 'prop-types';
import Subtitle from 'components/Titles/Subtitle';
import Card from 'components/Card';
import {
  DescriptionWrapStl,
  DescContentStl,
  ItemsRightStl,
} from './styled';

const Description = ({
  icon, description, title, unSelected, onClick,
}) => (
  <>
    <Card icon={icon} onClick={() => onClick(null)} />
    <DescriptionWrapStl>
      <Subtitle>{title}</Subtitle>
      <DescContentStl>
        {description}
      </DescContentStl>
    </DescriptionWrapStl>
    <ItemsRightStl>
      {unSelected.map((card) => (
        <Card
          onClick={() => onClick(card)}
          size="80px"
          key={card.title}
          {...card}
          title=""
        />
      ))}

    </ItemsRightStl>
  </>
);

Description.propTypes = {
  icon: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  unSelected: PropTypes.array.isRequired,
};

export default Description;
