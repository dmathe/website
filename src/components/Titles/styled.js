import styled from 'styled-components';

const Title = styled.h1`
  color: #6c8aaf;
`;

export default Title;
