import styled from 'styled-components';
import theme from 'global-styles';

export default styled.h1`
  font-family: ${theme.fontFamily.h};
  font-style: normal;
  font-size: 3em;
  line-height: 65px;
  letter-spacing: 0.5px;
  color: ${theme.colors.title};
  text-align: ${({ textAlign }) => (textAlign || 'center')};
`;
