import styled from 'styled-components';
import theme from 'global-styles';

export default styled.h2`
  width: 100%;
  font-size: 2em;
  text-align: center;
  margin-bottom: 50px;
  color: ${theme.colors.darkBlue};
`;
