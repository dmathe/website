import styled, { css } from 'styled-components';
import styles from 'global-styles';

const sizes = {
  s: () => ({
    width: '270px',
    height: '50px',
  }),
  m: () => ({
    width: '380px',
    height: '60px',
  }),
  l: () => ({
    width: '490px',
    height: '70px',
  }),
};

const StyledButtonSelection = styled.button`
  ${({ size }) => size && sizes[size]};
  border: 1px solid ${styles.colors.middBlue};
  background: white;
  margin: ${({ margin }) => margin};
  display: flex;
  flex-flow: row;
  justify-content: space-between;
  align-items: center;
  border-radius: 4px;
  ${({ selected }) => selected && css`
      background: ${styles.colors.middBlue};
      cursor: pointer;
      box-shadow: 0 2px 4px 1px lightgrey;
    };
  `};

  &:hover {
    background: ${styles.colors.middBlue};
    cursor: pointer;
    box-shadow: 0 2px 4px 1px lightgrey;
  };
`;

const Name = styled.span`
  font-family: 'Changa';
  font-style: normal;
  font-weight: normal;
  font-size: 21px;
  line-height: 21px;
  letter-spacing: 0.5px;
  color: ${styles.colors.labelBlue};
  margin: 0 auto;
  ${StyledButtonSelection}:hover & {
    color: white;
  };
${({ selected }) => selected && css`
    color: white;
  };
`};
`;

export { StyledButtonSelection, Name };
