import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Svg from 'components/Svg';
import ArrowRightSvg from 'images/arrowRight.svg';
import CheckWhiteSvg from 'images/checkWhite.svg';
import { StyledButtonSelection, Name } from './styled';

// svg: svg file imported
// size: size in px.
// margin: margin in px.
// name: text display on the button.
// arrow: display an arrow right svg at the right.
// check: display an check/valid svg at the right.
// onClick: execute onClick function.
// selected: is selected or not

const ButtonSelection = ({
  svg, name, size, arrow, check, margin, onClick, selected,
}) => (
  <StyledButtonSelection selected={selected} onClick={onClick} margin={margin} size={size}>
    <Svg name={`${name}Svg`} src={svg} size={23} />
    <Name selected={selected}>{name}</Name>
    {arrow && <Svg name="arrowRightSvg" src={ArrowRightSvg} size={17.5} />}
    {check && <Svg name="checkSvg" src={CheckWhiteSvg} size={25} />}
  </StyledButtonSelection>
);

ButtonSelection.propTypes = {
  svg: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  size: PropTypes.string,
  margin: PropTypes.string,
  arrow: PropTypes.bool,
  selected: PropTypes.bool,
  check: PropTypes.bool,
  onClick: PropTypes.func,
};

ButtonSelection.defaultProps = {
  size: 's',
  margin: '20px',
  arrow: false,
  check: false,
  selected: false,
  onClick: () => {},
};

export default memo(ButtonSelection);
