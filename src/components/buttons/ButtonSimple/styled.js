import styled, { css } from 'styled-components';
import styles from 'global-styles';

const ButtonSimpleStl = styled.button`
  width: 275px;
  height: 55px;
  background: ${styles.colors.white};
  border: 5px solid ${styles.colors.labelBlue};
  border-radius: 45px;
  
  &:disabled {
    color: ${styles.colors.disabled} !important;
    border: 5px solid ${styles.colors.disabled} !important;
  }

  ${({ disabled }) => !disabled && css`
  &:hover {
    cursor: pointer;
    background: ${styles.colors.labelBlue};
    box-shadow: lightgrey 0 2px 5px 2px;
  }`};
`;

const TextButtonSimpleStl = styled.span`
  font-family: 'Roboto Bold';
  font-size: 2em;
  line-height: 41px;
  letter-spacing: 0.5px;
  color: ${({ disabled }) => (
    disabled
      ? styles.colors.disabled
      : styles.colors.labelBlue
  )};

  ${({ disabled }) => !disabled && css`

    ${ButtonSimpleStl}:hover & {
      color: white;
    }`};
`;

const ButtonWrapStl = styled.div`
  display: flex;
  justify-content: ${({ align }) => align || 'center'};
  margin: ${({ margin }) => margin || '10px'};
`;

export {
  ButtonSimpleStl,
  TextButtonSimpleStl,
  ButtonWrapStl,
};
