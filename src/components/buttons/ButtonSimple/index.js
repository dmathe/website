import React from 'react';
import PropTypes from 'prop-types';
import { ButtonSimpleStl, TextButtonSimpleStl, ButtonWrapStl } from './styled';

const ButtonSimple = ({
  txt, type, align, margin, disabled,
}) => (
  <ButtonWrapStl margin={margin} align={align}>
    <ButtonSimpleStl disabled={disabled} type={type}>
      <TextButtonSimpleStl disabled={disabled}>{txt}</TextButtonSimpleStl>
    </ButtonSimpleStl>
  </ButtonWrapStl>
);

ButtonSimple.propTypes = {
  txt: PropTypes.string.isRequired,
  type: PropTypes.string,
  align: PropTypes.string,
  margin: PropTypes.string,
  disabled: PropTypes.bool,
};

ButtonSimple.defaultProps = {
  type: 'button',
  align: 'center',
  margin: '10px',
  disabled: false,
};

export default ButtonSimple;
