import React from 'react';
import PropTypes from 'prop-types';
import { ButtonWrapStl } from '../ButtonSimple/styled';
import { ButtonLinkStl, TextButtonLinkStl } from './styled';

const ButtonLink = ({
  txt, type, align, margin, to,
}) => (
  <ButtonWrapStl margin={margin} align={align}>
    <ButtonLinkStl to={to} type={type}>
      <TextButtonLinkStl>{txt}</TextButtonLinkStl>
    </ButtonLinkStl>
  </ButtonWrapStl>
);

ButtonLink.propTypes = {
  txt: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
  type: PropTypes.string,
  align: PropTypes.string,
  margin: PropTypes.string,
};

ButtonLink.defaultProps = {
  type: 'button',
  align: 'center',
  margin: '10px',
};

export default ButtonLink;
