import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const ButtonLinkStl = styled(Link)`
  width: 275px;
  height: 55px;
  background: #FFFFFF;
  border: 5px solid #6C8AAF;
  border-radius: 45px;
  box-sizing: border-box;
  text-align: center;
  padding: 1px 6px;

  &:hover {
    cursor: pointer;
    background: #6C8AAF;
    box-shadow: lightgrey 0 2px 5px 2px;
  }
`;

export const TextButtonLinkStl = styled.span`
  font-family: 'Roboto Bold';
  font-size: 2em;
  line-height: 41px;
  letter-spacing: 0.5px;
  color: #6C8AAF;

  ${ButtonLinkStl}:hover & {
    color: white;
  }
`;
