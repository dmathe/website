import React from 'react';
import renderer from 'react-test-renderer';
import Input from '..';

const props = {
  label: '', type: '', name: '', value: '', onChange: () => {},
};

describe('Input', () => {
  test('snapshot renders', () => {
    const component = renderer.create(<Input {...props} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
