import styled from 'styled-components';
import styles from 'global-styles';

export const Wrap = styled.div`
  width: 100%;
  margin: 10px;
`;

export const LabelStl = styled.label`
  position: relative;
  display: flex;
  flex-direction: column;
  text-align: initial;
  font-family: 'Changa';
  font-size: 17px;
  line-height: 26px;
  letter-spacing: 0.5px;
  color: ${styles.colors.labelBlue};
`;

export const InputStl = styled.input`
  margin: 3px 0;
  height: 42px;
  background: ${styles.colors.white};
  border: 2px solid ${styles.colors.labelBlue};
  box-sizing: border-box;
  border-radius: 4px;
  padding: 10px;
  ${({ value }) => (value && `background-color: ${styles.colors.lightBlue}`)};
`;

export const TextAreaStl = styled.textarea`
  margin: 5px 0;
  height: 200px;
  background: ${styles.colors.white};
  border: 2px solid ${styles.colors.labelBlue};
  box-sizing: border-box;
  border-radius: 4px;
  padding: 10px;
`;

export const Asterix = styled.div`
  color: #A0CBFF;
`;

export const RowStl = styled.div`
  display: flex;
`;

export const SvgStl = styled.div`
  position: absolute;
  right: 20px;
  bottom: 5px;
`;
