import React, { memo } from 'react';
import PropTypes from 'prop-types';
import {
  Wrap, LabelStl, InputStl, TextAreaStl, Asterix, RowStl,
} from './styled';

const Input = memo(({
  label, type, name, value, onChange,
}) => (
  <Wrap>
    <LabelStl htmlFor={name}>
      <RowStl>
        {label}
        <Asterix>&nbsp;*</Asterix>
      </RowStl>
      {type === 'text'
      && (
      <InputStl
        type={type}
        name={name}
        value={value}
        onChange={({ target }) => onChange(target.name, target.value)}
        placeholder={label}
      />
      )}
      {type === 'textarea'
      && (
      <TextAreaStl
        name={name}
        value={value}
        onChange={({ target }) => onChange(target.name, target.value)}
        placeholder={label}
      />
      )}
    </LabelStl>
  </Wrap>
), (pP, nP) => pP.value === nP.value);

Input.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string,
};

Input.defaultProps = {
  type: 'text',
};

export default Input;
