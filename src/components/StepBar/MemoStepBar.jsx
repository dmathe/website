import React, { memo } from 'react';
import { SELECT_STEP } from 'containers/StepYourNeeds/actions';
import PropTypes from 'prop-types';
import { Line, Nbr } from './styled';

const MemoStepBar = ({
  totalStep, isDisable, selected, i, dispatch,
}) => (
  <>
    <Nbr
      disable={isDisable}
      selected={selected}
      onClick={() => !isDisable && dispatch({ type: SELECT_STEP, index: i })}
    >
      {i}
    </Nbr>
    {i !== totalStep && <Line />}
  </>
);

MemoStepBar.propTypes = {
  totalStep: PropTypes.number.isRequired,
  isDisable: PropTypes.bool.isRequired,
  selected: PropTypes.bool.isRequired,
  i: PropTypes.number.isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default memo(MemoStepBar);
