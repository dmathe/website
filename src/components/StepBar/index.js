import React, { useContext } from 'react';
import StepContext from 'containers/StepYourNeeds/Context';
import { Wrapper } from './styled';
import MemoStepBar from './MemoStepBar';


const getStepBar = ({ totalStep, currentStep, startedStep }, dispatch) => {
  const Component = [];
  for (let i = 1; i <= totalStep; i += 1) {
    const isDisable = i > startedStep;
    const selected = currentStep === i;
    Component.push((
      <MemoStepBar
        key={i}
        totalStep={totalStep}
        isDisable={isDisable}
        selected={selected}
        i={i}
        dispatch={dispatch}
      />));
  }
  return Component;
};

const StepBar = () => {
  const { state, dispatch } = useContext(StepContext);

  return (
    <Wrapper>
      {getStepBar(state, dispatch)}
    </Wrapper>
  );
};

export default StepBar;
