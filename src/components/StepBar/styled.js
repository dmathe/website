import styled from 'styled-components';

export const Wrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const Nbr = styled.div`
    font-family: 'Changa';
    text-align: center;
    color: ${({ selected }) => (selected ? 'white' : '#6C8AAF')};
    ${({ selected }) => selected && 'background-color: #A6C1E2'};
    font-size: 2.2em;
    width: 50px;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    border: 1px solid;
    cursor: ${({ disable }) => (disable ? 'not-allowed' : 'pointer')};
`;

export const Line = styled.div`
    width: 100px;
    margin: 0 25px;
    height: 1px;
    background-color: #6C8AAF;
`;
