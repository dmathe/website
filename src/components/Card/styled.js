import styled from 'styled-components';
import theme from 'global-styles';

export const Wrapper = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  font-family: ${theme.fontFamily.smallH}
`;

export const ItemStl = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${theme.colors.lightBlue};
  border: 1px solid ${theme.colors.darkBlue};
  max-width: 100%;
  max-height: 100%;
  transform: rotate(45deg);
  cursor: pointer;
  border-radius: 10px;
  ${(({ size }) => size && `
  width: ${size};
  height: ${size};`
  )}
  &:hover {
    box-shadow: 0px 0px 10px lightgrey;
  }
`;

export const ContentStl = styled.img`
  display: flex;
  align-items: center;
  justify-content: center;
  transform: rotate(-45deg);
  border-radius: 10px;
  max-width: 80%;
  max-height: 80%;
`;
