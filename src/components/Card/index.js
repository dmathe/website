import React from 'react';
import PropTypes from 'prop-types';
import Subtitle from 'components/Titles/Subtitle';
import {
  Wrapper,
  ItemStl,
  ContentStl,
} from './styled';


// eslint-disable-next-line no-unused-vars
const Card = ({
  title, icon, onClick, size,
}) => (
  <Wrapper
    onClick={onClick}
    size={size}
  >
    <Subtitle>{title}</Subtitle>
    <ItemStl size={size}>
      <ContentStl alt={title} src={icon} />
    </ItemStl>
  </Wrapper>
);

Card.defaultProps = {
  onClick: () => {},
  title: '',
  size: '180px',
};


Card.propTypes = {
  title: PropTypes.string,
  size: PropTypes.string,
  icon: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

export default Card;
