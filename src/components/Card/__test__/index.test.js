import React from 'react';
import { shallow } from 'enzyme';
import Card from 'components/Card';
import diamond from 'images/diamond.svg';

describe('Card', () => {
  test('snapshot renders', () => {
    Card.defaultProps.onClick();
    const onClick = jest.fn();
    const component = shallow(<Card onClick={onClick} icon={diamond} />);
    component.find('Wrapper').simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});
