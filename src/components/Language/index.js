import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { locales } from 'translations';

const Select = styled.select`
  position: absolute;
  bottom: 5px;
  left: 5px;
  z-index: 2;
  background: none;
  font-size: 16px;
`;

const Language = ({ handleSelect, defaultValue }) => (
  <Select onChange={handleSelect} defaultValue={defaultValue}>
    {locales.map((l) => (
      <option key={l}>{l}</option>
    ))}
  </Select>
);

Language.propTypes = {
  handleSelect: PropTypes.func.isRequired,
  defaultValue: PropTypes.string.isRequired,
};

export default Language;
