import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Svg from 'components/Svg';
import closeCrossWhite from 'images/closeCrossWhite.svg';
import { StyledTagBoxShadow, StyledTagClipPath, TagName } from './styled';

const Tag = ({
  tagsName, value, onChange, tagsList,
}) => {
  const [hover, setHover] = useState(false);

  const handleTag = (val) => {
    let newTags = tagsList;
    if (newTags.includes(val)) {
      newTags = newTags.filter((tag) => tag !== val);
    } else newTags.push(val);
    onChange(tagsName, newTags);
  };

  return (
    <StyledTagBoxShadow
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <StyledTagClipPath>
        <TagName>{value.toUpperCase()}</TagName>
        {hover && (
        <Svg
          onClick={() => handleTag(value)}
          name="closeCrossTag"
          src={closeCrossWhite}
          size={14}
          cursor="pointer"
        />
        )}
      </StyledTagClipPath>
    </StyledTagBoxShadow>
  );
};

Tag.propTypes = {
  tagsName: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  tagsList: PropTypes.array.isRequired,
};

export default Tag;
