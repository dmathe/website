import styled from 'styled-components';
import styles from 'global-styles';

const StyledTagBoxShadow = styled.div`
  filter: drop-shadow(0px 2px 2px #cccccc);
  height: 35px;
  margin: 5px 10px;
`;

const StyledTagClipPath = styled.div`
  display: flex;
  flex-flow: row;
  justify-content: flex-start;
  align-items: center;
  width: 130px;
  height: 35px;
  background: ${styles.colors.middBlue};
  border-radius: 3px;
  padding: 0 20px;
  clip-path: polygon(92% 0%, 100% 50%, 92% 100%, 0% 100%, 0 49%, 0% 0%);
`;

const TagName = styled.span`
  font-family: 'Changa';
  font-style: normal;
  font-size: 20px;
  line-height: 21px;
  width: 110px;
  text-overflow: ellipsis;
  overflow: hidden;
  letter-spacing: 0.5px;
  color: #FFFFFF;
`;

export { StyledTagBoxShadow, StyledTagClipPath, TagName };
