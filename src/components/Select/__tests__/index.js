import React from 'react';
import renderer from 'react-test-renderer';
import { mount, shallow } from 'enzyme';
import Select from '../index';

const props = {
  value: 'a',
  onChange: jest.fn(),
  handleTag: jest.fn(),
  dataList: ['a'],
  label: 'label',
  name: 'name',
};

describe('Select', () => {
  test('snapshot', () => {
    const component = renderer.create(<Select {...props} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
  test('testing onBlur', () => {
    const component = shallow(<Select {...props} />);
    component.find('Wrap').simulate('blur');
  });
  test('testing onFocus mousedown', () => {
    const component = mount(<Select {...props} />);
    component.find('Wrap').simulate('focus');
    component.find('OptionList').simulate('mousedown');
    expect(props.onChange).toHaveBeenCalled();
  });
  test('testing onFocus mousedown selected', () => {
    const component = mount(<Select {...props} tagsName="a" tags={['a']} />);
    component.find('Wrap').simulate('focus');
    component.find('OptionList').simulate('mousedown');
    expect(component).toMatchSnapshot();
  });
  test('testing Input onChange', () => {
    const component = shallow(<Select {...props} />);
    component.find('InputSelectStl').simulate('change', { target: { value: 'val', name: 'name' } });
    component.find('InputSelectStl').simulate('click');
  });
});
