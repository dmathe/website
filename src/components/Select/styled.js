import styled from 'styled-components';
import styles from 'global-styles';

export const InputSelectStl = styled.input`
  height: 42px;
  background: ${styles.colors.white};
  border: 2px solid ${styles.colors.labelBlue};
  box-sizing: border-box;
  padding: 10px;
  margin: 3px 0 0 0;
  border-radius: ${({ open }) => (open ? '4px 4px 0 0' : '4px')};
  ${({ value }) => (value
    ? `background-color: ${styles.colors.lightBlue}`
    : '')};
`;

export const DataListStl = styled.div`
  display: ${({ open }) => (open ? 'flex' : 'none')};
  width: 100%;
  height: 180px;
  overflow-y: scroll;
  background: ${styles.colors.white};
  border: 2px solid ${styles.colors.labelBlue};
  width: initial;
  border-top: none;
  border-radius: 0 0 4px 4px;
  flex-direction: column;
  text-align: initial;
  max-height: 180px;
  height: auto;
`;

export const OptionList = styled.div`
  padding: 7px 7px 7px 14px;
  margin-bottom: 2px;
  cursor: pointer;
  min-height: 30px;
  display: inline-flex;
  align-items: center;
  justify-content: space-between;
  &:hover {
    background-color: ${styles.colors.selectBlue};
  }
  ${({ isSelected }) => (isSelected && `background-color: ${styles.colors.selectBlue}`)};
`;

export const TextList = styled.span`
  ${OptionList}:hover & {
    color: ${styles.colors.white};
  }
  color: ${({ isSelected }) => (isSelected
    ? `${styles.colors.white}` : `${styles.colors.darkBlue}`)};
`;
