import React, { useState, memo } from 'react';
import PropTypes from 'prop-types';
import ArrowDown from 'images/arrow-down.svg';
import CheckWhite from 'images/checkWhite.svg';
import Svg from 'components/Svg';
import {
  Wrap, LabelStl, Asterix, RowStl, SvgStl,
} from '../Input/styled';

import {
  InputSelectStl, TextList, DataListStl, OptionList,
} from './styled';

// dataList: liste a afficher dans les options
// label: label de l'input
// value: valeur de l'input
// onChange: function qui update la value du state avec l'obj en param
// name: nom de l'etat qui va s'update via "onChange"
// tagsList: (optionnel) liste de tags selectionne
// tagsName: (optionnel) nom de l'etat qui va s'update via "onChange"

const Select = ({
  dataList, label, value, onChange, name, tagsList, tagsName,
}) => {
  const [open, setOpen] = useState(false);

  const handleDisplay = (bool) => setOpen(bool);
  const formatDataList = !value
    ? dataList
    : dataList.filter((data) => data.toLowerCase().includes(value.toLocaleLowerCase()));

  const handleTag = (val) => {
    let newTags = [...tagsList];
    if (newTags.includes(val)) {
      newTags = newTags.filter((tag) => tag !== val);
    } else newTags.push(val);
    onChange(tagsName, newTags);
  };
  return (
    <Wrap
      onFocus={() => handleDisplay(true)}
      onBlur={() => handleDisplay(false)}
    >
      <LabelStl htmlFor={name}>
        <RowStl>
          {label}
          <Asterix>&nbsp;*</Asterix>
        </RowStl>
        <InputSelectStl
          onChange={({ target }) => onChange(target.name, target.value)}
          value={value}
          placeholder={label}
          open={open}
          autoComplete="off"
          name={name}
        />
        <SvgStl>
          <Svg name="arrow-down" src={ArrowDown} size={13} />
        </SvgStl>
      </LabelStl>
      <DataListStl open={open}>
        {formatDataList.map((el) => {
          const isSelected = tagsList.includes(el);
          return (
            <OptionList
              onMouseDown={() => {
                if (tagsName) {
                  handleTag(el);
                } else onChange(name, el);
              }}
              key={el}
              isSelected={isSelected}
            >
              <TextList isSelected={isSelected}>{el}</TextList>
              {isSelected && <Svg name="checkWhite" src={CheckWhite} size={25} />}
            </OptionList>
          );
        })}
      </DataListStl>
    </Wrap>
  );
};

Select.propTypes = {
  label: PropTypes.string.isRequired,
  dataList: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  tagsList: PropTypes.array,
  tagsName: PropTypes.string,
};

Select.defaultProps = {
  tagsList: [],
  tagsName: '',
};

export default memo(Select);
