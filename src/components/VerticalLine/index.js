import React from 'react';
import PropTypes from 'prop-types';
import {
  StyledVerticalLineContainer,
  StyledVerticalLine,
  StyledVerticalLineValue,
} from './styled';

// size: ['s', 'm', 'l'] (70px, 140px, 210px)

const VerticalLine = ({ value, size, color }) => (
  <StyledVerticalLineContainer>
    <StyledVerticalLine size={size} color={color} />
    <StyledVerticalLineValue color={color}>{value}</StyledVerticalLineValue>
  </StyledVerticalLineContainer>
);

VerticalLine.propTypes = {
  value: PropTypes.string.isRequired,
  size: PropTypes.string.isRequired,
  color: PropTypes.string,
};

VerticalLine.defaultProps = {
  color: 'lightgrey',
};

export default VerticalLine;
