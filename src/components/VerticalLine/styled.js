import styled from 'styled-components';

const sizes = {
  s: ({
    height: '70px',
  }),
  m: ({
    height: '140px',
  }),
  l: ({
    height: '210px',
  }),
};

const StyledVerticalLineContainer = styled.div`
  display: flex;
  flex-flow: column;
  align-items: center;
  height: fit-content;
  margin: 5px 10px;
`;

const StyledVerticalLine = styled.div`
  width: 1px;
  ${({ size }) => size && sizes[size]};
  background: ${({ color }) => (color || 'lightgrey')};
  margin: 5px 0;
`;

const StyledVerticalLineValue = styled.span`
  color: ${({ color }) => (color || 'lightgrey')};
  font-family: 'Roboto';
`;

export {
  StyledVerticalLineContainer,
  StyledVerticalLine,
  StyledVerticalLineValue,
};
