import styles from '../global-styles';

describe('global-styles', () => {
  test('snapshot renders', () => {
    expect(styles).toMatchSnapshot();
  });
});
