import React from 'react';
import renderer from 'react-test-renderer';
import ErrorBoundary from '../ErrorBoundary';

describe('ErrorBoundary', () => {
  test('snapshot renders', () => {
    const component = renderer.create(<ErrorBoundary><div /></ErrorBoundary>);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  // cause error

  // test('snapshot error renders', () => {
  //   const component = renderer.create(
  //     <ErrorBoundary><div>{new Error()}</div></ErrorBoundary>,
  //   );
  //   const tree = component.toJSON();
  //   expect(tree).toMatchSnapshot();
  // });
});
