import React from 'react';
// import renderer from 'react-test-renderer';
import routes from 'routes';
import { IntlProvider } from 'react-intl';

describe('routes', () => {
  test('snapshot renders', () => {
    expect(
      <IntlProvider locale="fr">
        {routes}
      </IntlProvider>,
    ).toMatchSnapshot();
  });
});
