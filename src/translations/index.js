const messages = {
  en: {
    Nav1: 'Who we are',
    Nav2: 'Your needs',
    Nav3: 'Join us',
    Nav4: 'Contact',
  },
  fr: {
    Nav1: 'Qui sommes-nous',
    Nav2: 'Vos besoins',
    Nav3: 'Rejoignez-nous',
    Nav4: 'Contact',
  },
};

const locales = ['en', 'fr'];

export { messages, locales };
