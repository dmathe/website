import Main from 'containers/Main';
import StepYourNeeds from 'containers/StepYourNeeds';

const routes = [
  {
    name: 'Main',
    path: '/',
    component: Main,
  }, {
    name: 'StepYourNeeds',
    path: '/vos-besoins',
    component: StepYourNeeds,
  },
];

export default routes;
