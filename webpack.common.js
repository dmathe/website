const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackTemplate = require('html-webpack-template');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: { app: './src/index.js' },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: { presets: ['@babel/env'] },
      },
      {
        test: /\.css$/,
        include: path.resolve(__dirname, 'src'),
        use: ['style-loader',
          MiniCssExtractPlugin.loader,
          'css-loader'],
      },
      {
        test: /\.(jpe?g|png|gif|svg|woff(2)?|ttf|eot)$/i,
        include: path.resolve(__dirname, 'src'),
        use: [
          'url-loader?limit=10000', 'img-loader',
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      inject: false,
      title: 'Whis',
      template: HtmlWebpackTemplate,
      hash: true,
      filename: 'index.html',
      appMountId: 'root',
      meta: [{
        viewport: 'width=device-width, initial-scale=1.0',
      }],
    })],
  resolve: {
    extensions: ['*',
      '.js',
      '.jsx'],
    symlinks: false,
    modules: [
      path.resolve('./src'), path.resolve('./node_modules'),
    ],
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: '[name].bundle.[hash].js',
    chunkFilename: '[id].bundle.[hash].js',
    publicPath: '/',
  },
};
